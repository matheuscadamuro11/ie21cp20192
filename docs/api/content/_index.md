# Documentação do Projeto

## Descrição
O projeto consiste na rotação de servos motores, de modo que a face desejada sempre fique voltada para o ponto com maior captação de luz, tendo como intuito otimizar a captação de luz solar por placas fotovoltaicas.

## Lista de Materiais
01 - Protoboard.<br />
01 - Arduino UNO.<br />
02 - Sensores de luminosidade LDR 5mm<br />
01 - Servo motor //completar com o modelo

<iframe src="https://www.youtube.com/watch?v=wZZ7oFKsKzY" frameborder="0" allowfullscreen="true">