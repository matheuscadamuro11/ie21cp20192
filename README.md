# Introdução

O projeto consiste em um dispositivo que facilita de forma pratica a obtenção de medidas de distâncias, obtidas por meio de um sensor ultrassônico, que serão exibidas em um display digital.

# Objetivos

O trabalho surgiu com o proposito de reduzir o tempo levado na realização de medições.

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Matheus D. Cadamuro|@matheuscadamuro11|
|Tais M. Riquinho|@taismanicariquinho|
|Luiz E. Kramer|@LuizKramer|
|Luiz A. Silva dos Santos|@Luizalexandre212|

# Documentação

A documentação do projeto pode ser acessada pelo link:

> https://gitlab.com/matheuscadamuro11/html

Link site html:

> https://matheuscadamuro11.gitlab.io/html/